<?php
/*en este ejercicio ahora los atributos son de tipo privados y para poder acceder a estos datos creamos el constructor*/
class Curso {

	private $titulo;
	private $profesor;
	private $duracion;
	private $costo;
	private $disponible;

	#creando el constructor
	public function __construct($titulo, $profesor, $duracion, $costo, $disponible){

		$this->titulo = $titulo;
		$this->profesor = $profesor;
		$this->duracion = $duracion;
		$this->costo = $costo;
		$this->disponible = $disponible;
	}


	#Encapsulación
	# getter - Setter
	/* Getter para obtener datos
	Setter para setear o asignar datos*/

	public function obtenerTitulo(){
		return $this->titulo;
	}

	public function obtenerPofesor(){
		return $this->profesor;
	}

	public function asignarTitulo($titulo){
		$this->titulo = $titulo;
	}
}

#gracias a estas ultimas funciones podemos obtener de manera correcta el valor del titulo y el profesor segun el objeto que sea invocado en el archivo index

?>