<?php

class Curso {

	public $publico = 'Público: POO Avanzado I';
	private $privado = 'privado: POO Avanzado I';
	protected $protegido = 'Protegido: POO Avanzado I';

	function obtenerMensajePrivado(){
		return $this->privado;
	}

}

$prueba = new Curso();
echo "<p>".$prueba->publico."</p>";
echo "<p>".$prueba->obtenerMensajePrivado()."</p>";

/* El ejercicio muestra como crear una clase con atributos de tipo público, privado y protegido; luego se instancia la clase creando un nuevo objeto
el cual puede acceder al atributo privado mediante la función obtenerMensajePrivado*/
?>