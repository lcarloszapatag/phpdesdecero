<?php
/* 
-------------------------------------------------------Arreglos-----------------------------------------------------------

    Definicion: un arreglo es una colección finita de datos múltiples ordenados en filas 
    unidimensionales(lo que llamabamos vectores) o multidimensionales(lo que llamabamos matrices) 
    se trata de un tipo de dato compuesto.

    Pueden ser indexados y asociativos
    Indexados: aquellos que utilizan indices que controlan las posiciones y comienzan en 0
*/

    //declarando 

    $numeros=[]; //opción 1

    $numeros2= array();// opción 2

    $personajes=['Mario', 'Wario', 'Peach', 'Yoshi', 'Luigi'];
    var_dump($personajes); //forma correcta de mostrar el arreglo si lo hacemos con un echo mostrara error
    echo "<br />";
    echo $personajes[3]; // para mostrar lo que se encuentra en un indice del arreglo si podemos usar echo
    echo "<br />";
    print_r($personajes); // otra forma de mostrar el arreglo

/* 
    Arreglos Asociativos: indican una relación con una llave (muy usados con bases de datos)
*/
    $personajes2=['Personaje1'=>'Mario','Personaje2'=>'Wario','Personaje3'=>'Peach','Personaje4'=>'Yoshi','Personaje1'=>'Luigi'];

    $personajes3=array('Personaje1'=>'Mario','Personaje2'=>'Wario','Personaje3'=>'Peach','Personaje4'=>'Yoshi','Personaje1'=>'Luigi');

/* 
    Arreglos Multidimensionales:
    aquellos arreglos formados tanto por una columna como por una fila
    en su declaración o uso, primero posicionan la fila y luego la columna
*/
    $multinumeros=[0][0];// fila cero columna cero

/* 
    para agregar un nuevo elemento indexado a mi arreglo lo puedo hacer así:
*/
    $personajes[]='Bowser';// lo pondra al final del vector donde hay un indice libre
    $personajes[10]='Toad';// o tambien podemos decirle en cual posición queremos que aparezca

    //var_dump($personajes);

    /*--------------------------------- creación de arreglo asociativo-----------------------------------------------*/
    echo "<br />";
    $usuario=['Nombre'=>'Wario','vehiculo'=>'Tubiturbo','Arma'=>'Rayo'];
    var_dump($usuario);

    echo "<br />";
    $usuario2= array('Nombre'=>'DK. Kong','vehiculo'=>'Cuatrimoto','Arma'=>'Banana');
    var_dump($usuario2);
    
    //para imprimir un elemento del array asociativo lo llamamos por su llave así:

    echo "<br />";
    echo "El nombre del usuario es: ".$usuario['Nombre'];

    /*--------------------------------- creación de arreglo multidimensional-----------------------------------------------*/
    echo "<br />";
    // forma uno usando la palabra reservada array
    $marioKart=array(
        array('Wario',33,'kart normal'),
        array('Luigi',20,'Tubiturbo'),
        array('Dk. Kong',25,'cuatrimoto')
    );
    var_dump($marioKart);
    
    echo "<br />";
    //forma abreviada sin la palabra reservada array y con corchetes[]
    $marioKart2=[
        ['Wario',33,'kart normal'],
        ['Luigi',20,'Tubiturbo'],
        ['Dk. Kong',25,'cuatrimoto']
    ];
    var_dump($marioKart2);

//arreglo multidimensional asociativo

$mariokart3 = array(
    array('nombre'=>'Peach', 'edad'=>6, 'circuito'=>"Luigi's mansion"),
    array('nombre'=>'koopa', 'edad'=>12, 'circuito'=>"Precipicios"),
    array('nombre'=>'wario', 'edad'=>10, 'circuito'=>"wario stadium")
);

//si le quiero asignar otro campo y valor:

$mariokart3[2]['velocidad']='Match 20';

echo "-------";

var_dump($mariokart3);
?>
