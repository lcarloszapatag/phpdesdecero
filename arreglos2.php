<?php
/* 
    arreglos con funciones nativas
*/

$cadena='';
$arreglo=[];
$marioKart = ['Wario', 'Toad', 'Bowser', 'Mario', 'Luigi', 'Peach'];
var_dump($marioKart);
echo "<br />";

#Vacio -empty
var_dump(empty($arreglo)); //devuelve true
var_dump(empty($marioKart));//devuelve false
echo "<br />";

#para saber si el elemento existe - isset
var_dump(isset($marioKart[50]));//devuelve false ya que no hay elemento en esa posición
echo "<br />";

#Para contar elementos del arreglo
echo count($marioKart);
echo "<br />";

#Antepenúltimo elemento
$posicion= count($marioKart)-2;
echo $marioKart[$posicion];
echo "<br />";

/*-----------------------------------Ordenando elementos de un array */

#por orden alfabetico - sort
sort($marioKart);
var_dump($marioKart);
echo "<br />";

#para mantener el orden alfabético pero conservando el indice -asort
asort($marioKart);
var_dump($marioKart);
echo "<br />";

#para ordenar de manera inversa - rsort
rsort($marioKart);
var_dump($marioKart);
echo "<br />";

#para ordenar de manera inversa sin perder el indice inicial - arsort
arsort($marioKart);
var_dump($marioKart);
echo "<br />";

/*Operaciones aritméticas con arrays */

#sumar los valores del arreglo:
$numeros=[1,3,50,21,5,8];
$suma= array_sum($numeros);
echo "la suma de los numeros del array es: ".$suma;
echo "<br />";

#encontrar la diferencia entre arreglos
$salonA=['a1'=>'Juan','a2'=>'Susana', 'a3'=>'Homero', 'a4'=>'Jaime'];
$salonB=['a1'=>'Santiago','a2'=>'Diego', 'a3'=>'Susana'];

$diferencia =array_diff($salonA, $salonB);
var_dump($diferencia);

?>